package az.haweb.demo.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class HelloWorldController {

    final static Logger logger = LoggerFactory.getLogger(HelloWorldController.class);


    @Value("${app.name}")
    private String name;


    @GetMapping("/name")
    public String getName() {
        logger.info("Application name : " + this.name);
        logger.info("/name requested , response : " + this.name);
        return this.name;
    }

    @GetMapping("/publicip")
    public String getIp() {
        logger.info("Application name : " + this.name);
        RestTemplate restTemplate = new RestTemplate();
        String publicip = restTemplate.getForObject("http://ipecho.net/plain", String.class);
        logger.info("/publicip requested , response : " + publicip);
        return publicip;
    }

    @GetMapping("/app")
    public String getRequestParams(@RequestParam(name = "p1",required = false) String param1,
                                   @RequestParam(name = "p2",required = false) String param2) {
        logger.info(this.name);
        return param1 + " : " + param2;
    }


}
