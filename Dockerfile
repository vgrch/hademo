FROM openjdk:8-jre-alpine

WORKDIR /app

# Setting timezone
RUN apk add --no-cache tzdata
ENV TZ Asia/Baku
ENV APPNAME APPLICATION1

RUN mkdir logs
ADD config config
ADD target/*.jar app.jar

ENTRYPOINT ["java", "-jar","-Dapp.name=${APPNAME}"]
CMD ["app.jar"]
